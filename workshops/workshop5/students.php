<?php
require('./functions.php');


$conn = getConnection();
$userNumber = $argv[1];
if ($userNumber == null) {
    echo "el parametro enviado esta vacio";
    die;
}
$sql = "SELECT id, name, lastname, cedula, age FROM students LIMIT $userNumber";
$result = $conn->query($sql);
$count = 0;


if ($result->num_rows > 0) {
    // output data of each row
    while ($row = $result->fetch_assoc()) {
        echo $row["id"] . ", " . $row["name"] . ", " . $row["lastname"] . ", " . $row["cedula"] . ", " . $row["age"] . "\n";
    }
} else {
    echo "0 results";
}

$conn->close();
