<?php

function getConnection()
{
    $connection = new mysqli('localhost:3306', 'root', 'Otoya01-07-97', 'php_web2');
    if ($connection->connect_errno) {
        printf("Connect failed: %s\n", $connection->connect_error);
        die;
    }
    return $connection;
}

function getUser()
{
    $conn = getConnection();
    $sql = "SELECT * FROM users";
    $result = $conn->query($sql);

    if ($conn->connect_errno) {
        $conn->close();
        return [];
    }
    $conn->close();
    return $result;
}

function deleteUser($id)
{
    $conn = getConnection();
    $sql = "DELETE FROM users WHERE id = $id";
    $result = $conn->query($sql);

    if ($conn->connect_errno) {
        $conn->close();
        return false;
    }
    $conn->close();
    return true;
}
