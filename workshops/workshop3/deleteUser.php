<?php
include('functions.php');

$id = $_GET['id'];
if ($id) {
    $user = getUser($id);
    if ($user) {
        $deleted = deleteUser($id);
        if ($deleted) {
            header('Location: /?status=success');
        } else {
            header('Location: /?status=error');
        }
    } else {
        header('Location: /?status=error');
    }
} else {
    header('Location: /list.php');
}
